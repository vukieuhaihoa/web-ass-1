$(document).ready(function () {
  $(window).on("load", function () {
    $(".loader-wrapper").delay(900).fadeOut("slow");
  })
  $(window).scroll(function () {
    if ($(this).scrollTop() > 40) {
      $("#gotop").fadeIn();
    }
    else {
      $("#gotop").fadeOut();
    }
  });

  $("#gotop").click(function () {
    $('html ,body').animate({ scrollTop: 0 }, 800);
  });

});